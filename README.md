<div align="center">
<h1>BASH SNIPPETS </h1></div>

This is README for bash-snippets for Visual Studio Code. It will help you write faster bash scirpts.

**This repository is still in work in progress so some functions might changed in the future**

# Instalation
### With marketplace
To install this extension go to Visual Studio Code marketplace and search for `bash-snippets` and click `install` to add it to your code editor.

### Manually
It is possible to install this extension manually. You can download zip file with code of this repository or clone it with: 
```bash
# $ this is add to show you that this is console command, don't copy it to console
$ git clone https://gitlab.com/Czesiek2000/bash-snippets.git

```

After you clone it, unzip and find this path `<your-main-drive>:\Users\<your-user-name>\.vscode\extensions` and place it inside this direcory.

NOTE: Values inside `<>` are just placeholders, so replace it with your values.

HINT: Default value of main drive is in Windows `C`

### MacOs and Linux manually instalation
If your MacOs or Linux user you should consider using marketplace install option. 

# Usage
This snippet use aliases for creating snippets.

## Example: 
Assume that you want forget to add shebang (`#!/bin/bash`), simply you need to do:
![shebang](./image1.png)

Or create basic for loop just type:
![forloop](./image2.png)

# Release Notes
1.0.0 </br>
* Init release of bash-snippet.

NOTE: After every operation remeber to click `TAB` or `ENTER`

If you :heart: this repository don't forget to leave a :star:, Thanks :punch: